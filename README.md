# My DD Inventory

My DD Inventory is a way to display your Dragon Dice (TM) from [SFR, Inc.](https://www.sfr-inc.com) collection online using the file format generated from the export of [Rolls of Esfah](https://codeberg.org/caranmegil/rolls-of-esfah).

## Installation

It's painfully simple.  You just clone the repo into your web server directly.  Then, copy the exported file from Rolls of Esfah onto the one in the data directly using the same file name.  Give it any appropriate permissions necessary to load as required by your web server (I'm looking at you, Apache).  Done!  Visit your page and it'll have a scrolling list of your collection!